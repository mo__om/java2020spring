package java2020spring;


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicComboBoxUI;

/*本面板构思制作完全由本人制作，参考了一些资料，一共有三个面板，基本框架是以东西南北中的面板
目的是为了缩放以后都可以拥有很好的显示效果，具体细节请参考下列注释*/    //创作者：李宪栋
//*主类开始
public class FrameBane implements ActionListener,PopupMenuListener{    //  PopupMenuListener可以让下拉框被选择后无颜色背景         
	 JTextField display;
	 JTextField display1;
	 JTextField displayZh;
	 JTextField displayZh1;
	 JComboBox<String> comboBox;
	 JComboBox<String> comboBox1;
	 JPanel panel;
	 JPanel panelJi;
	 JPanel panelSum;
	 JPanel panelZh;
	 CardLayout card;
	 JTextPane displayZh3;  //JTextPane可以插入图片
	 FrameBane() {

//！！！	
//		
//1.一个无原始框且可拖拽缩放的类win10计算器窗口
		final JFrame frame = new JFrame();
		frame.setUndecorated(true);         //去除标题框，会造成无法移动和无法缩放。
		Border b = new CompoundBorder(new EtchedBorder(), new LineBorder(Color.BLACK, 0));//一个粗细为0的蚀刻边界，CompoundBorder将两个边界混合
		card=new CardLayout();
		panel = new JPanel();
		panel.setLayout(card);
		panel.setSize(660,800);        //加面板是为了有边界
		panel.setBorder(b);             //底层面板声明的结束
		
		panelJi=new JPanel();       //三个显示面板的声明
		panelSum=new JPanel();
		panelZh=new JPanel();
		panelJi.setLayout(new BorderLayout());
		panelJi.setSize(660,800);        //加面板是为了有边界
		//panelJi.setBorder(b);
		panelSum.setLayout(new BorderLayout());
		panelSum.setSize(660,800);        //加面板是为了有边界
		//panelSum.setBorder(b);
		panelZh.setLayout(new BorderLayout());
		panelZh.setSize(660,800);        //加面板是为了有边界
		//panelZh.setBorder(b);

		panel.addMouseMotionListener(new MouseAdapter() {        //给整个面板加监视器，使其可以拖拽缩放
			private boolean top = false;
			private boolean down = false;
			private boolean left = false;
			private boolean right = false;       //四个常量分别是鼠标是否在四条边上                
			private Point draggingAnchor = null;       //该坐标可以记录当准备拖拽时鼠标距离窗口原点的坐标位置

			/*鼠标光标方法*/public void mouseMoved(MouseEvent e) {        
				if (e.getY() <=4 ) {                                 //获取光标的距离窗口原点y的位置，
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));       //当小于4个像素时
					top = true;                                                       //光标会改变形状，top为真，接下面解析
				} else if (Math.abs(e.getY() - frame.getSize().getHeight()) <= 4) {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
					down = true;
				} else if (e.getX() == 4) {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
					left = true;
				} else if (Math.abs(e.getX() - frame.getSize().getWidth()) <= 4) {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
					right = true;
				} else {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					draggingAnchor = new Point(e.getX(), e.getY() ); //当鼠标位置不在四边上时，记录要移动窗口
					top = false;                    //一瞬间的鼠标相对于窗口的位置，接下面解析
					down = false;
					left = false;
					right = false;

				}

			}

			/*鼠标拖拽方法*/public void mouseDragged(MouseEvent e) {
				Dimension dimension = frame.getSize();
				if (top) {                 //接上面解析，此时这个动作是移动上边，分别setSize和set位置，dimension.getWidth()为窗口
					dimension.setSize(dimension.getWidth(), dimension.getHeight() - e.getY()); //宽度不变，e.getY()为拖拽后
					frame.setSize(dimension);         //鼠标相对于窗口的位置，可能正可能负，getLocationOnScreen().y 是在屏幕上位置，两者注意区分
					frame.setLocation(frame.getLocationOnScreen().x, frame.getLocationOnScreen().y + e.getY());
				} else if (down) {
					dimension.setSize(dimension.getWidth(), e.getY());
					frame.setSize(dimension);

				} else if (left) {
					dimension.setSize(dimension.getWidth() - e.getX(), dimension.getHeight());
					frame.setSize(dimension);
					frame.setLocation(frame.getLocationOnScreen().x + e.getX(), frame.getLocationOnScreen().y);
				} else if (right) {
					dimension.setSize(e.getX(), dimension.getHeight());
					frame.setSize(dimension);
				} else {
					frame.setLocation(e.getLocationOnScreen().x - draggingAnchor.x,
							e.getLocationOnScreen().y - draggingAnchor.y);  //移动后鼠标在屏幕的位置减去记录下来的距离
				}
			}
		});
//窗口的完成
//
//!!!		

		
		
////////////////////////////////以上是最底层的面板，以下是计算器面板////////////////////////////////////
	
		
		
//!!!
//		
//2.1North板标题栏的完成 X轴两对
	/*North板的声明与监视器的声明*/
		ChangeButton listen=new ChangeButton();  //当鼠标进入按钮时按钮发生颜色改变的监听器
		FrameFunction listenF=new FrameFunction();  //最大化最小化退出监视器
		JPanel topPanel=new JPanel();         //
		topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.X_AXIS));   //标题栏布局
		
	/*标题盒子*/
		Box boxL=Box.createVerticalBox();
		JLabel title=new JLabel("  栋的计算器");
		title.setFont(new Font("宋体", Font.BOLD, 15));       
		boxL.add(title);                                          //标题盒子
		
	/*三按键盒子*/ //调用了1.1的ChangeButton颜色改变的监听器和2.1的FrameFunction最大化最小化退出监视器
		Box boxR = Box.createHorizontalBox();
		JButton button1 = new JButton(" -");
		JButton button2 = new JButton(" □");
		JButton button3 = new JButton(" ×");
		button1.setFont(new Font("宋体", Font.PLAIN, 28));
		button2.setFont(new Font("宋体", Font.PLAIN, 16));
		button3.setFont(new Font("宋体", Font.PLAIN, 21));//字体设置
		button1.setContentAreaFilled(false);      //变透明
		button1.setBorderPainted(false);          //去除边界
		button1.setFocusPainted(false);           //按下按键时不会有难看的方框
		button2.setContentAreaFilled(false);
		button2.setBorderPainted(false);
		button2.setFocusPainted(false);
		button3.setContentAreaFilled(false);
		button3.setBorderPainted(false);
		button3.setFocusPainted(false);
		button1.addMouseListener(listen);
		button2.addMouseListener(listen);
		button3.addMouseListener(listen);         //ChangeButton监视器即进入按钮改变颜色监视器的加入
		button1.addActionListener(listenF);       //FrameFunction监视器即最大化最小化退出监视器的加入
		button2.addActionListener(listenF);
		button3.addActionListener(listenF);
		listenF.setFrame(frame);            //FrameFunction监视器把面板调过去以供最大化最小化退出的实现
		boxR.add(button1);
		boxR.add(Box.createHorizontalStrut(3));          //按键间的距离
		boxR.add(button2);
		boxR.add(Box.createHorizontalStrut(1));
		boxR.add(button3);                                    //三按键盒子
       
	/*标题盒子和三按键盒子加入North面板*/
		topPanel.add(boxL);
		topPanel.add(Box.createHorizontalGlue());
		topPanel.add(boxR);
		panelJi.add(topPanel,BorderLayout.NORTH);
//North板标题栏完成
//
//!!!
		

		
//2.2Center面板的扩展键 Y轴两个
	/*Center面板的声明*/
		JPanel centerPanel=new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel,BoxLayout.Y_AXIS));       //中间面板
	
	/*扩展键的三个盒子*/	
		Box boxN= Box.createHorizontalBox();  //装下面两个
		
		//#M键和次标准//
		Box boxNL=Box.createHorizontalBox();
		JButton function=new JButton("←");
		function.setFont(new Font("华文琥珀", Font.BOLD, 36));
		function.setContentAreaFilled(false);
		function.setBorderPainted(false);   
		function.setFocusPainted(false);
	    function.addMouseListener(listen);     //鼠标进入按钮颜色变化监视器          
		JLabel mention=new JLabel("次标准");
		mention.setFont(new Font("宋体", Font.BOLD, 26));
		function.addActionListener(this);
		boxNL.add(function);
		boxNL.add(Box.createHorizontalStrut(5));
		boxNL.add(mention);                                     
	
		//黑暗模式
		Box boxNR=Box.createHorizontalBox();
		//JButton more=new JButton("^Dark");
		//more.setFont(new Font("宋体", Font.PLAIN, 21));  //可能不加了
		//boxNR.add(more);
		
		//合并
		boxN.add(boxNL);
		boxN.add(Box.createHorizontalGlue());
		boxN.add(boxNR);                               //三个盒子的完成
		
	/*TextField的盒子与面板*/		
		SuanFa suanFa=new SuanFa();     //算法监视器
		Box boxS=Box.createVerticalBox();
		JPanel smallText=new JPanel();    //这个面板是给小数字展示条用的
		smallText.setLayout(new BoxLayout(smallText,BoxLayout.X_AXIS));
		
		//两个文本条的建立
		display1=new JTextField();
		display=new JTextField(80);
		display1.setOpaque(false);          //使其变透明
		display1.setEditable(false);                  //只读
		display1.setForeground(new Color(203,185,251));    //不可以直接往里面打字,同时里面字体为黑色，是不是傻了眼    我原本
		display1.setBorder(new EmptyBorder(0,2,0,0));  //去除边框        设为2以后就足够长
		display1.setFont(new Font("微软雅黑", Font.PLAIN, 30)); //里面字体的设置
		display1.setHorizontalAlignment(JTextField.RIGHT);       //向右对齐
		display1.setText("0");           //初始为0
		display.setOpaque(false);          //使其变透明
		display.setEditable(false);                  //只读
		display.setForeground(Color.BLACK);    //同时里面字体为黑色，是不是傻了眼
		display.setBorder(new EmptyBorder(0,2,0,0));  //去除边框，设为2以后就足够长
		display.setFont(new Font("微软雅黑", Font.PLAIN, 85)); //里面字体的设置
		display.setHorizontalAlignment(JTextField.RIGHT);       //向右对齐
		display.setText("0");           //初始为0
		smallText.add(display1);   //加入面板
		boxS.add(smallText);
		smallText.setPreferredSize(new Dimension(10, 2));  //小文本条的大小
		boxS.add(display);        //文本条的完成
		suanFa.setplay(display);           //把面板传过去
		suanFa.setplay1(display1);          //把面板传过去
		
		
	/*扩展键的三个盒子和TextField的盒子加入center板*/	
		centerPanel.add(boxN);
		centerPanel.add(Box.createVerticalStrut(5));
		centerPanel.add(boxS);
		panelJi.add(centerPanel,BorderLayout.CENTER);
//center板完成
//
//!!!

		
		
		
//2.3South面板即NumberPane，调用了NumberPane方法建立了板
		NumberPane numberPane=new NumberPane();
		numberPane.setSuanFa(suanFa);
		numberPane.setNumberPane();
		numberPane.setPreferredSize(new Dimension(150, 500));   //给这个设置默认时south就可以够长了
		panelJi.add(numberPane,BorderLayout.SOUTH);
//
//		
//!!!		

		
		
//////////////////////////////////////以上是计算器面板，以下是开始选择面板////////////////////////////////////////////	

		
		
//!!!
//
//3.1一个panelSum为东西南北布局，北边放标题栏，中间放三个按钮，基本是抄自己上面的	
		JPanel topPanelSum=new JPanel();         
		topPanelSum.setLayout(new BoxLayout(topPanelSum,BoxLayout.X_AXIS));   //标题栏布局
		
	/*标题盒子*/
		Box boxLSum=Box.createVerticalBox();
		JLabel titleSum=new JLabel("  栋的多功能器");
		titleSum.setFont(new Font("宋体", Font.BOLD, 15));       
		boxLSum.add(titleSum);                                          //标题盒子
		
	/*三按键盒子*/ //调用了ChangeButton颜色改变的监听器和的FrameFunction最大化最小化退出监视器
		Box boxRSum = Box.createHorizontalBox();
		JButton button1Sum = new JButton(" -");
		JButton button2Sum = new JButton(" □");
		JButton button3Sum = new JButton(" ×");
		button1Sum.setFont(new Font("宋体", Font.PLAIN, 28));
		button2Sum.setFont(new Font("宋体", Font.PLAIN, 16));
		button3Sum.setFont(new Font("宋体", Font.PLAIN, 21));//字体设置
		button1Sum.setContentAreaFilled(false);      //变透明
		button1Sum.setBorderPainted(false);          //去除边界
		button1Sum.setFocusPainted(false);           //按下按键时不会有难看的方框
		button2Sum.setContentAreaFilled(false);
		button2Sum.setBorderPainted(false);
		button2Sum.setFocusPainted(false);
		button3Sum.setContentAreaFilled(false);
		button3Sum.setBorderPainted(false);
		button3Sum.setFocusPainted(false);
		button1Sum.addMouseListener(listen);
		button2Sum.addMouseListener(listen);
		button3Sum.addMouseListener(listen);         //ChangeButton监视器即进入按钮改变颜色监视器的加入
		button1Sum.addActionListener(listenF);       //FrameFunction监视器即最大化最小化退出监视器的加入
		button2Sum.addActionListener(listenF);
		button3Sum.addActionListener(listenF);
		listenF.setFrame(frame);            //FrameFunction监视器把面板调过去以供最大化最小化退出的实现
		boxRSum.add(button1Sum);
		boxRSum.add(Box.createHorizontalStrut(3));          //按键间的距离
		boxRSum.add(button2Sum);
		boxRSum.add(Box.createHorizontalStrut(1));
		boxRSum.add(button3Sum);                                    //三按键盒子
       
	/*标题盒子和三按键盒子加入North面板*/
		topPanelSum.add(boxLSum);
		topPanelSum.add(Box.createHorizontalGlue());
		topPanelSum.add(boxRSum);
		panelSum.add(topPanelSum,BorderLayout.NORTH);
		
		
    /*中部面板*///ridLayout(5,5)布局
		JPanel center=new JPanel();
		center.setLayout(new GridLayout(5,5));
		JButton buttonJi=new JButton("计算器");
		JButton buttonZh=new JButton("转换器");
		buttonJi.setFont(new Font("华文琥珀", Font.PLAIN, 32));
		buttonZh.setFont(new Font("华文琥珀", Font.PLAIN, 32));
		buttonJi.setContentAreaFilled(false);      //变透明
		buttonJi.setBorderPainted(false);          //去除边界
		buttonJi.setFocusPainted(false);           //按下按键时不会有难看的方框
		buttonZh.setContentAreaFilled(false);      //变透明
		buttonZh.setBorderPainted(false);          //去除边界
		buttonZh.setFocusPainted(false);           //按下按键时不会有难看的方框
		buttonJi.addActionListener(this);
		buttonZh.addActionListener(this);
		buttonZh.addMouseListener(listen);
		buttonJi.addMouseListener(listen);
	
		for(int i=0;i<5;i++){                  //逐一添加到格子中
			for(int j=0;j<5;j++){
				if(i==1&&j==1){
					center.add(buttonJi);
				}
				else if(i==3&&j==3){
					center.add(buttonZh);
				}
				else if(i==2&&j==2){
					JButton mid=new JButton("..LXD..");
					mid.setContentAreaFilled(false); 
					mid.setBorderPainted(false);          //去除边界
					mid.setFocusPainted(false);
					mid.setFont(new Font("华文琥珀", Font.PLAIN, 30));
					mid.setBackground(new Color(153,253,175));
					center.add(mid);
				}
				else
					center.add(new JLabel(""));
			}
		}
		panelSum.add(center,BorderLayout.CENTER);     //中间面板加到中部
//panelSum总选择面板完成	
//		
//!!!	
		
		
		
////////////////////////////////以上是开始选择面板，以下是转换器面板////////////////////////////////////////////		
	
		
		
		
//!!!
//
//4.1一个panelSum为东西南北布局，北边放标题栏，基本就是计算器的翻版，基本是抄自己上面的	
		JPanel topPanelZh=new JPanel();         
		topPanelZh.setLayout(new BoxLayout(topPanelZh,BoxLayout.X_AXIS));   //标题栏布局
		
	/*标题盒子*/
		Box boxLZh=Box.createVerticalBox();
		JLabel titleZh=new JLabel("  栋的转换器");
		titleZh.setFont(new Font("宋体", Font.BOLD, 15));       
		boxLZh.add(titleZh);                                          //标题盒子
		
	/*三按键盒子*/ //调用了ChangeButton颜色改变的监听器和FrameFunction最大化最小化退出监视器
		Box boxRZh = Box.createHorizontalBox();
		JButton button1Z = new JButton(" -");
		JButton button2Z = new JButton(" □");
		JButton button3Z = new JButton(" ×");
		button1Z.setFont(new Font("宋体", Font.PLAIN, 28));
		button2Z.setFont(new Font("宋体", Font.PLAIN, 16));
		button3Z.setFont(new Font("宋体", Font.PLAIN, 21));//字体设置
		button1Z.setContentAreaFilled(false);      //变透明
		button1Z.setBorderPainted(false);          //去除边界
		button1Z.setFocusPainted(false);           //按下按键时不会有难看的方框
		button2Z.setContentAreaFilled(false);
		button2Z.setBorderPainted(false);
		button2Z.setFocusPainted(false);
		button3Z.setContentAreaFilled(false);
		button3Z.setBorderPainted(false);
		button3Z.setFocusPainted(false);
		button1Z.addMouseListener(listen);
		button2Z.addMouseListener(listen);
		button3Z.addMouseListener(listen);         //ChangeButton监视器即进入按钮改变颜色监视器的加入
		button1Z.addActionListener(listenF);       //FrameFunction监视器即最大化最小化退出监视器的加入
		button2Z.addActionListener(listenF);
		button3Z.addActionListener(listenF);
		listenF.setFrame(frame);            //FrameFunction监视器把面板调过去以供最大化最小化退出的实现
		boxRZh.add(button1Z);
		boxRZh.add(Box.createHorizontalStrut(3));          //按键间的距离
		boxRZh.add(button2Z);
		boxRZh.add(Box.createHorizontalStrut(1));
		boxRZh.add(button3Z);                                    //三按键盒子
       
	/*标题盒子和三按键盒子加入North面板*/
		topPanelZh.add(boxLZh);
		topPanelZh.add(Box.createHorizontalGlue());
		topPanelZh.add(boxRZh);
		panelZh.add(topPanelZh,BorderLayout.NORTH);
//北部面板完成
//
//!!!
		
		
//4.2Center面板的扩展键 Y轴两个
		/*Center面板的声明*/
		    ZhuanSuanFa suanFaZhuan=new ZhuanSuanFa();        //输入数字时的算法
		    ZhuanListen Zlisten1=new ZhuanListen();           //将第一个下拉框改变时的算法
		    ZhuanListen2 Zlisten2=new ZhuanListen2();         //将第二个下拉框改变时的算法
			JPanel centerPanelZ=new JPanel();                      
			centerPanelZ.setLayout(new BoxLayout(centerPanelZ,BoxLayout.Y_AXIS));       //中间面板
		
		/*扩展键的三个盒子*/	
			Box boxNZ= Box.createHorizontalBox();  //装下面两个
			
			//#M键和次标准//
			Box boxNLZ=Box.createHorizontalBox();
			JButton functionZ=new JButton("←");
			functionZ.setFont(new Font("华文琥珀", Font.BOLD, 36));
			functionZ.setContentAreaFilled(false);
			functionZ.setBorderPainted(false);   
			functionZ.setFocusPainted(false);
		    functionZ.addMouseListener(listen);     //鼠标进入按钮颜色变化监视器          
			JLabel mentionZ=new JLabel("仅长度");
			mentionZ.setFont(new Font("宋体", Font.BOLD, 26));
			functionZ.addActionListener(this);
			boxNLZ.add(functionZ);
			boxNLZ.add(Box.createHorizontalStrut(5));
			boxNLZ.add(mentionZ);                                     
		
			//黑暗模式
			Box boxNRZ=Box.createHorizontalBox();
			//JButton more=new JButton("^Dark");
			//more.setFont(new Font("宋体", Font.PLAIN, 21));  //可能不加了
			//boxNR.add(more);
			
			//合并
			boxNZ.add(boxNLZ);
			boxNZ.add(Box.createHorizontalGlue());
			//boxNZ.add(boxNRZ);                               //三个盒子的完成
					
		
			
		/*文本条和下拉框*/
			Box boxsZh=Box.createVerticalBox();
			Box boxNLZ2=Box.createHorizontalBox();
			Box boxNLZ3=Box.createHorizontalBox();         
			
			displayZh=new JTextField();
			displayZh1=new JTextField();
			displayZh3=new JTextPane();  //JTextPane可以插入图片
			comboBox=new JComboBox<String>();
			comboBox.addItem("米");
			comboBox.addItem("公里");
			comboBox.addItem("英寸");
			comboBox.addItem("英尺");
			comboBox.addItem("码");
			comboBox.addItem("海里");
			comboBox1=new JComboBox<String>();
			comboBox1.addItem("米");
			comboBox1.addItem("公里");
			comboBox1.addItem("英寸");
			comboBox1.addItem("英尺");
			comboBox1.addItem("码");
			comboBox1.addItem("海里");            //文本条和下拉框的说明
		
			
			displayZh3.setMaximumSize(new Dimension(1000000,13000000));   //当在boxlayout中，要设定组件的大小需使用setMaximumSize
			comboBox.setMaximumSize(new Dimension(100,20000));
			comboBox1.setMaximumSize(new Dimension(100,20000));
			comboBox.setBorder(BorderFactory.createEmptyBorder());
			comboBox.setPopupVisible(false);
			
			displayZh1.setOpaque(false);          //使其变透明
			displayZh1.setEditable(false);                  //只读
			displayZh1.setForeground(Color.black);    //不可以直接往里面打字,同时里面字体为黑色，是不是傻了眼    我原本
			displayZh1.setBorder(new EmptyBorder(0,2,0,0));  //去除边框        设为2以后就足够长
			displayZh1.setFont(new Font("Yu Gothic UI Semilight", Font.PLAIN, 60)); //里面字体的设置
			displayZh1.setText("0");           //初始为0
			
			displayZh.setOpaque(false);          //使其变透明
			displayZh.setEditable(false);                  //只读
			displayZh.setForeground(Color.black);    //不可以直接往里面打字,同时里面字体为黑色，是不是傻了眼    我原本
			displayZh.setBorder(new EmptyBorder(0,2,0,0));  //去除边框        设为2以后就足够长
			displayZh.setFont(new Font("微软雅黑", Font.PLAIN, 62)); //里面字体的设置
			displayZh.setText("0");           //初始为0
			
			displayZh3.setOpaque(false);          //使其变透明
			displayZh3.setEditable(false);                  //只读
			displayZh3.setForeground(Color.black);    //不可以直接往里面打字,同时里面字体为黑色，是不是傻了眼    我原本
			displayZh3.setBorder(new EmptyBorder(0,2,0,0));  //去除边框        设为2以后就足够长
			displayZh3.setFont(new Font("微软雅黑", Font.PLAIN, 17)); //里面字体的设置
			displayZh3.setText(" \n");
			
			comboBox1.setFont(new Font("微软雅黑", Font.BOLD, 18));
			comboBox1.setBorder(new EmptyBorder(0,0,0,0)); 
			comboBox1.setOpaque(false); 
			comboBox.setFont(new Font("微软雅黑", Font.BOLD, 18));
			comboBox.setBorder(new EmptyBorder(0,0,0,0)); 
			comboBox.setOpaque(false);                            //同上
			comboBox.setUI(new BasicComboBoxUI());           //这个是让下拉框的边界消失
			comboBox1.setUI(new BasicComboBoxUI());
			comboBox.addPopupMenuListener( this );   
			comboBox1.addPopupMenuListener( this );   //下拉框选择完以后不会有灰色背景
			
			
			
			Zlisten1.setBox1(comboBox);
			Zlisten2.setBox2(comboBox1);
			comboBox.addItemListener(Zlisten1);
			comboBox1.addItemListener(Zlisten2);
			Zlisten1.setJtext1(displayZh);
			Zlisten1.setJtext2(displayZh1);
			Zlisten1.setJtext3(displayZh3);
			Zlisten2.setJtext1(displayZh);
			Zlisten2.setJtext2(displayZh1);
			Zlisten2.setJtext3(displayZh3);
			suanFaZhuan.setJtext1(displayZh);
			suanFaZhuan.setJtext2(displayZh1);
			suanFaZhuan.setJtext3(displayZh3);
			Zlisten1.setListen(Zlisten2);
			Zlisten1.setSuanFa(suanFaZhuan);
			Zlisten2.setListen(Zlisten1);
			Zlisten2.setSuanFa(suanFaZhuan);                  //这里是三个监视器里的各种调用
			
			
			
			boxNLZ2.add(comboBox);
			boxNLZ2.add(Box.createHorizontalGlue());         //让它靠右
			
			boxNLZ3.add(comboBox1);
			boxNLZ3.add(Box.createHorizontalGlue());        //让它靠右
			
			
			boxsZh.add(displayZh);
			boxsZh.add(boxNLZ2);
			boxsZh.add(displayZh1);
			boxsZh.add(boxNLZ3);
			boxsZh.add(Box.createVerticalStrut(10));
			boxsZh.add(displayZh3);                                  //把该加的东西加进去
			
			
			
			/*扩展键的三个盒子和文本的盒子加入center板*/	
			centerPanelZ.add(boxNZ);
			centerPanelZ.add(Box.createVerticalStrut(1));
			centerPanelZ.add(boxsZh);
			panelZh.add(centerPanelZ,BorderLayout.CENTER);
			
//center板完成
//
//!!!
			
			
				
//4.3South面板即NumberPane，调用了NumberPaneZh方法见建立了板
			NumberPaneZh numberPaneZh=new NumberPaneZh();
			//numberPaneZh.setSuanFa(suanFa);
			numberPaneZh.setNumberPane();
			numberPaneZh.setPreferredSize(new Dimension(100, 400));   //给这个设置默认时south就可以够长了
			for(int i=0;i<15;i++)
				numberPaneZh.button2[i].addActionListener(suanFaZhuan);    //这里是把按钮都调过来加监视器，我不知道为什么计算器的setsuanfa在这没作用
			panelZh.add(numberPaneZh,BorderLayout.SOUTH);
//键盘板完成
//			
//!!!					

		
		
		
////////////////////////////////以上是转换器面板，以下是底层面板总结////////////////////////////////////////////				


			
		
//!!!
//		
//5.frame板的最后声明
		panel.add("Calculator",panelJi);
		panel.add("Sum",panelSum);
		panel.add("Change",panelZh);
		frame.setLocation(200, 100);
		frame.add(panel);
		frame.setSize(660,800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.show();
		/*changePanel.setCard(card);
		changePanel.setJipanel(panelJi);
		changePanel.setPanel(panel);
		changePanel.setSumpanel(panelSum);*/
		card.show(panel,"Sum");
		
//
//		
//!!!
		
	}//*主面板方法结束
	
	 
//*切换面板方法	
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("←")){
			card.show(panel,"Sum");
		}
		else if(e.getActionCommand().equals("计算器")){
			card.show(panel,"Calculator");
		}
		else if(e.getActionCommand().equals("转换器")){
			card.show(panel,"Change");
		}
		
	}//*切换面板方法结束
//*下拉框选择后背景颜色为透明，这里是抄了网上的，要有一个Item接口
	 public void popupMenuCanceled( javax.swing.event.PopupMenuEvent e ){}

	    public void popupMenuWillBecomeVisible( javax.swing.event.PopupMenuEvent e )
	    {
	    	if( e.getSource() == null ){ return; }

	        javax.swing.ListCellRenderer comp =((JComboBox)e.getSource()).getRenderer();

	        if( comp instanceof javax.swing.JComponent ){ ( (javax.swing.JComponent)comp ).setOpaque( true ); }
	    }

	    public void popupMenuWillBecomeInvisible( javax.swing.event.PopupMenuEvent e )
	    {
	    	if( e.getSource() == null ){ return; }

	        javax.swing.ListCellRenderer comp =((JComboBox)e.getSource()).getRenderer();

	        if( comp instanceof javax.swing.JComponent ){ ( (javax.swing.JComponent)comp ).setOpaque( false ); }

	      
	    }
//*下拉框选择后背景颜色为透明方法结束


}
//*主类结束





//*辅助类
//！！！
//一.ChangeButton监视器即进入按钮改变颜色监视器
class ChangeButton extends MouseAdapter{
	String regex="[0-9]+";
	public void mouseEntered(MouseEvent e){
		JButton button=(JButton)e.getSource();
		if(button.getText()==" ×"){                    //刚开始我居然还想把该死的按钮传过来，因为getActionCommand没法用
			button.setContentAreaFilled(true);
			button.setBackground(Color.RED);
		}
		else if(button.getText()=="←"){                    //刚开始我居然还想把该死的按钮传过来，因为getActionCommand没法用
			button.setContentAreaFilled(true);
			button.setBackground(Color.lightGray);
		}
		else if(button.getText()==" -"||button.getText()==" □"){
			button.setContentAreaFilled(true);
			button.setBackground(Color.lightGray);
		}
		else if(button.getText()=="转换器"||button.getText()=="计算器"||button.getText()==""){
			button.setContentAreaFilled(true);
			if(button.getText()=="转换器"){
			   button.setBackground(new Color(147, 147, 255));
			}
			else
			   button.setBackground(new Color(153,253,175));
		}
		else{
			button.setContentAreaFilled(true);
			button.setBackground(new Color(153,253,175));
		}
	}
	public void mouseExited(MouseEvent e){            //鼠标退出按钮记得让它变回原样
		JButton button=(JButton)e.getSource();
	    if(button.getText()==" ×"||button.getText()==" -"||button.getText()==" □"||button.getText()=="←"){                   
	    	button.setContentAreaFilled(false);
		}
	    else if((button.getText()).matches(regex)||button.getText()=="+/-"||button.getText()=="."){
	    	button.setBackground(Color.WHITE);
	    }
	    else if(button.getText()=="="){
	    	button.setBackground(new Color(86, 90, 252));
	    }
	    else if(button.getText()=="转换器"||button.getText()=="计算器"||button.getText()==""){
	    	button.setContentAreaFilled(false);
	    }
	    else {
	    	button.setBackground(new Color(253, 247, 251)); 
	    }
	}
}
//
//！！！



//！！！
//一.二 转换器的ChangeButtonZh监视器,这里其实我只是想要让转换器的进入按钮颜色是蓝色为主底色，把上面整个复制过来是懒得再写了
class ChangeButtonZh extends MouseAdapter{
	String regex="[0-9]+";
	public void mouseEntered(MouseEvent e){
		JButton button=(JButton)e.getSource();
		if(button.getText()==" ×"){                    //刚开始我居然还想把该死的按钮传过来，因为getActionCommand没法用
			button.setContentAreaFilled(true);
			button.setBackground(Color.RED);
		}
		else if(button.getText()=="←"){                    //刚开始我居然还想把该死的按钮传过来，因为getActionCommand没法用
			button.setContentAreaFilled(true);
			button.setBackground(new Color(153,253,175));
		}
		else if(button.getText()=="转换器"||button.getText()=="计算器"||button.getText()==""){
			button.setContentAreaFilled(true);
			if(button.getText()=="转换器"){
			   button.setBackground(new Color(153,253,175));
			}
			else
			   button.setBackground(new Color(147, 147, 255));
		}
		else{
			button.setContentAreaFilled(true);
			button.setBackground(new Color(86, 90, 252));
		}
	}
	public void mouseExited(MouseEvent e){            //鼠标退出按钮记得让它变回原样
		JButton button=(JButton)e.getSource();
	    if(button.getText()==" ×"||button.getText()==" -"||button.getText()==" □"||button.getText()=="←"){                   
	    	button.setContentAreaFilled(false);
		}
	    else if((button.getText()).matches(regex)||button.getText()=="+/-"||button.getText()=="."){
	    	button.setBackground(Color.WHITE);
	    }
	    else if(button.getText()=="="){
	    	button.setBackground(new Color(86, 90, 252));
	    }
	    else if(button.getText()=="转换器"||button.getText()=="计算器"||button.getText()==""){
	    	button.setContentAreaFilled(false);
	    }
	    else {
	    	button.setBackground(new Color(253, 247, 251)); 
	    }
	}
}
//
//！！！


//！！！
//二.类最大化最小化退出监视器
class FrameFunction extends JFrame implements ActionListener{  //一定要JFrame子类，  才可以用frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	JFrame frame;
	public void setFrame(JFrame frame1){
		frame=frame1;                         //最开始忘记把面板发过来了，我才说怎么无法最大化最小化
	}
	
	public void actionPerformed(ActionEvent e) 
	    {
	        if(e.getActionCommand() == " □")
	        {
	        	     frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	                ((JButton)e.getSource()).setText(" O");
	        }

	        if(e.getActionCommand() == " O")
	        {
	        	     frame.setExtendedState(JFrame.NORMAL); 
	                ((JButton)e.getSource()).setText(" □");
	 
	        }

	        if(e.getActionCommand() == " -")
	        {
	        	frame.setExtendedState(JFrame.ICONIFIED); 
	        }


	        if(e.getActionCommand() == " ×")
	        {
	            System.exit(0);
	        }

	    }
}
//
//！！！



//！！！
//三.类 数字键盘的 调用了ChangeButton监视器即进入按钮改变颜色监视器的（计算器的）。
class NumberPane extends JPanel{
	SuanFa suanFa;   //算法监视器
	public void setSuanFa(SuanFa suanF){
		suanFa=suanF;
	}
	ChangeButton listen=new ChangeButton();  //监视器
	JButton []button2=new JButton[24];
	int i=0;
	public void setNumberPane(){
		setLayout(new GridLayout(6,4,2,2));     //后面两个就是空隙
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2)); //给整个面板加了空隙
		addButton("design by LXD");
		addButton("CE");
		addButton("C");
		addButton("＜dele");
		addButton("1/x");
		addButton("X方");
		addButton("X根");
		addButton("÷");
		addButton("7");
		addButton("8");
		addButton("9");
		addButton("×");
		addButton("4");
		addButton("5");
		addButton("6");
		addButton("-");
		addButton("1");
		addButton("2");
		addButton("3");
		addButton("+");
		addButton("+/-");
		addButton("0");
		addButton(".");
		addButton("=");
	}
	public void addButton(String s){
		JButton button=new JButton(s);
		String regex="[0-9]+";
		if(s.matches(regex)||s.equals("+/-")||s.equals(".")){
		button.setFont(new Font("华文琥珀", Font.PLAIN, 25));
		button.setBackground(Color.white);
		button.setBorderPainted(false);
		button.setFocusPainted(false);
		button.addMouseListener(listen);
		button.addActionListener(suanFa);
		button2[i++]=button;
			
		}
		else if(s.equals("=")){
			button.setFont(new Font("华文行楷", Font.BOLD, 28));
			button.setBackground(new Color(86, 90, 252));
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			button.addMouseListener(listen);
			button.addActionListener(suanFa);
			button2[i++]=button;
		}
		else if(s.equals("design by LXD")){
			button.setFont(new Font("方正舒体", Font.PLAIN, 18));
			button.setBackground(new Color(253, 247, 251));
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			button.addMouseListener(listen);
			button2[i++]=button;
		}
		
		else{
			button.setFont(new Font("华文新魏", Font.PLAIN, 21));
			//button.setContentAreaFilled(false);
			button.setBackground(new Color(253, 247, 251));  //这里可以打开画板参考
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			button.addMouseListener(listen);
			button.addActionListener(suanFa);
			button2[i++]=button;
		}
			
		add(button);
		suanFa.setKey(button2);
	}
}
//
//！！！

//！！！
//四.类 数字键盘的 调用了ChangeButtonZh监视器即进入按钮改变颜色监视器的（转换器的）
class NumberPaneZh extends JPanel{
	   
	ChangeButtonZh listen=new ChangeButtonZh();  //颜色监视器
	JButton []button2=new JButton[15];
	int i=0;
	public void setNumberPane(){
		setLayout(new GridLayout(5,3,2,2));     //后面两个就是空隙
		setBorder(BorderFactory.createEmptyBorder(2,2,2,2)); //给整个面板加了空隙。。。
		addButton("");
		addButton("CE");
		addButton("＜dele");
		addButton("7");
		addButton("8");
		addButton("9");
		addButton("4");
		addButton("5");
		addButton("6");
		addButton("1");
		addButton("2");
		addButton("3");
		addButton("design by LXD");
		addButton("0");
		addButton(".");


	}
	public void addButton(String s){
		JButton button=new JButton(s);
		String regex="[0-9]+";
		if(s.matches(regex)||s.equals(".")){
		button.setFont(new Font("华文琥珀", Font.PLAIN, 25));
		button.setBackground(Color.white);
		button.setBorderPainted(false);
		button.setFocusPainted(false);
		button.addMouseListener(listen);
		
		
		button2[i++]=button;
			
		}
		else if(s.equals("design by LXD")){
			button.setFont(new Font("方正舒体", Font.PLAIN, 18));
			button.setContentAreaFilled(false);
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			button2[i++]=button;
		}
		else if(s.equals("")){
			button.setContentAreaFilled(false);
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			
			button2[i++]=button;
		}
		
		else{
			button.setFont(new Font("华文新魏", Font.PLAIN, 21));
			button.setBackground(new Color(253, 247, 251));  //这里可以打开画板参考
			button.setBorderPainted(false);
			button.setFocusPainted(false);
			button.addMouseListener(listen);
			
			button2[i++]=button;
		}
		 
		add(button);
		
	}
}
//
//！！！





