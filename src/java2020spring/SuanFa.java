package java2020spring;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JTextField;

/*本算法未参考任何他人的思路或代码，从无到有百分百纯原创。                   //创作者：李宪栋
本算法主要的思想是通过变量lastNum记录上一次按钮是什么，通过与现在传过来的e.getActionCommand搭配做出下一步行动，如果情况过于复杂则设置足够多的变量来应对
即通过分析lastNum与e.getActionCommand的所有搭配情况，完成了算法本身
以下代码将会给出详细注释以供参考*/         //注释中的good没有实际意义，请忽略
public class SuanFa implements ActionListener {
	String regexN="[0123456789]+";              //正则表达式判断是不是数字+
	//String regexF="[0123]+";              //这个本来是加减乘除的正则表达式，但是我不会写
	JTextField display=new JTextField();            //接受传过来的文本条
	JTextField display1=new JTextField();           //接受传过来的文本条2
	double sum=0;                                  //记录开始计算后到目前为止已经计算后的储存的计算值
	double mid=0;                                  //主要用于计算时接受string转换为double充当第二个计算数
	String lastNum="0";                            //默认开始的上一个按钮是0
	boolean flag=true; 
	//flag的作用是：当按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到display1,也不会再同正常情况下一样进行计算，
	//当按下1+1=2后按下CE按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到display1,也不会再同正常情况下一样进行计算
	boolean flag3=true;   //当按下CE后此变量为false，大文本条变为0，此时若按下数字，会把0变成该数字，而不是加在0的后面
	boolean flag4=true;   //当按下=号后还可以按负根方，当小文本条最后一位为数字且小文本条不为0再按+-*/会把最新的计算结果推到小文本条中。
	boolean flag5=true;
	String Fmain;        //比如1+1，输入最后一个1后，+号被保存，以供下一个输入+或=号后进行计算
	String Smain;        //当lastNum为“.”时，若出现“98.”,保存“98”，之后按了“+”，会输出98+而不是98.+
	int i=0;             //用于set(i)Edit方法的循环
	JButton []button2=new JButton[24];     //接受传过来的按钮
	
//一些准备方法
	public void setplay(JTextField text){
		display=text;
	}
	public void setplay1(JTextField text){
		display1=text;
	}                                                         //传文本条过来
	public void setKey(JButton []key){
		button2=key;
	}                                                          //把按钮传过来
	public void setEdit(){
		for(i=0;i<24;i++){
			button2[i].setEnabled(true);
		}                                              //恢复按钮
	}
	public void setiEdit(){
		for(i=0;i<24;i++){
			if(i!=2)
			button2[i].setEnabled(false);
		}                                              //当输入错误，如分母为0，负数开根时按钮不可按
	}
	
	
	
//
//监视器方法开始	
	public void actionPerformed(ActionEvent e) {
		
		
//!!!
//
//	
	/*当你按下数字键时*/
		if(e.getActionCommand().matches(regexN)){
			if(flag3==true&&((display.getText()).equals("0")&&display1.getText().equals("0"))||lastNum.equals("无")||lastNum.equals("09")){    //这里其实就是操作不规范后被按了C后按数字
				//诸如按下CE键后直接按数字，自己开根以后还想按数字加上去这句话是老版本所以&&display1.getText().equals("0")
				//被我加上去是为了修复0+9会直接重置的bug，但是按了=后还想按数字加上去，都会重置一切
				display1.setText("0");
				sum=0;
				display.setText(e.getActionCommand());  //重置同时把按的数字加上去
				lastNum=e.getActionCommand();        //把按的数字当成lastNum
				flag=true;           //这里也要加，否则按1+1=后按CE后按8后按+9+9后出现++9 这里关键是按了=号后没有重置flag
                flag3=true;
                flag4=true;       //别忘了
				Fmain="";
				Smain="";      //虽然不太造成什么影响，但是还是加上吧,保险起见
					
			}
			else{         //如果操作规范的正常流程
				if(display.getText().equals("0")){
					flag3=false;                         //这里是修复被我加上去是为了修复9+03会直接重置的bug
				}
				if(lastNum.matches("[0123456789]{1}")||lastNum.equals(".")){ 
					//如果前一位是数字或者是“.",  [0123456789]{1}是为了避免按等号后的“09”，
					if(flag3){
						//当按下CE后此变量为false，大文本条变为0，此时若按下数字，会把0变成该数字，而不是加在0的后面
						display.setText(display.getText()+e.getActionCommand());//就把按的数字加到后面
						lastNum=e.getActionCommand();  //记录上一位按的数字，其实记录具体是什么数字不是特别重要，记录符号更有用些
					}
					else{
						display.setText(e.getActionCommand());
						flag3=true;    //别忘了
						lastNum=e.getActionCommand();  //记录上一位按的数字，其实记录具体是什么数字不是特别重要，记录符号更有用些
					}
				}
				else if(lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷")){
					//如果前一位是+-*/       //这里并没有储存现在输入的这个数，而是上一次的计算结果，把这个数放到大文本条，后面的+=算法会把它用于计算
					sum= Double.parseDouble(display.getText());
				                                   //这个其实
					display.setText(e.getActionCommand());           //把按下的数字展示出来替换掉原来的
					Fmain=lastNum;                                //把计算符号留起来
					lastNum=e.getActionCommand();                  //把按的数字当成lastNum
				}        
			}
		}
		
		
	
		
	/*当你按下+-×÷时*/
		else if (e.getActionCommand().equals("+") || e.getActionCommand().equals("-")
				|| e.getActionCommand().equals("×") || e.getActionCommand().equals("÷")) {
			if (lastNum.matches(regexN) || lastNum.equals("无") || lastNum.equals(".")) {
				// 如果上一个按钮是数字包括等号，开根，或者点时（甚至是按了负号切换键，但是负号切换键不进行lastNum的设置)
				if (lastNum.equals(".")) { // 如果是点，当lastNum为“.”时，若出现“98.”,保存“98”，之后按了“+”，会输出98+而不是98.+
					display.setText(Smain);  // 把文本条的“.”掉 ，即用没有.保存下来的Smain 
				}                                      
					if ((display1.getText()).equals("0")) { // 判断是不是第一次在小文本条展示
						display1.setText(display.getText() + e.getActionCommand()); // 把数字和计算符号放到小文本条
						lastNum = e.getActionCommand(); // 这个不要漏了
					} 
					else {
						// 不是第一次展示，正常流程
						if (flag) {
							// 这里是true，正常流程，当按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到display1,
						    if(Fmain.equals("÷")&&display.getText().matches("[0.]+")){
						    	display.setText("分母为0错.请按C");          
								setiEdit();
						    }
						    else{
						    display1.setText(display1.getText() + (display.getText() + e.getActionCommand()));
							// 比如小文本条是1+，大文本条是1，这时候按“+“，小文本条就会变成1+1+
							mid = Double.parseDouble(display.getText()); // 把大文本条的1变成Double类型
						    
							if (Fmain.equals("+")) { // 刚刚上一步保存的计算符号,sum也是上一步保存的。。
								sum = sum + mid;
							} else if (Fmain.equals("-")) {
								sum = sum - mid;
							} else if (Fmain.equals("×")) {
								sum = sum * mid;
							} else if (Fmain.equals("÷")) {
								sum = sum / mid;
							} // 计算
							if (String.valueOf(sum).length() >= 8) {
								// 如果计算结果长度大于8
								BigDecimal sumM = new BigDecimal(sum);
								sum = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue(); // 保留小数点后6位
								if ((sum) % 1.0 == 0) { // 如果小数点后全是0，则输出没有0的
									display.setText(String.valueOf((long) (sum)));
								} 
								else {
									display.setText(String.valueOf(sum)); // 不全是0，则保留小数点后6位输出
								}
							} 
							else {
								// 如果长度没大于8，这里其实是必要的这样写的，跟上面不一样
								if ((sum) % 1.0 == 0) { // 如果小数点后全是0，则输出没有0的
									display.setText(String.valueOf((long) (sum)));
								} 
								else { // 不全是0，则保留小数点后6位输出
									display.setText(String.valueOf(sum));
								}
							}
							lastNum = e.getActionCommand(); // 别忘了
						}
						} 
						else {
							// 当按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到小文本条,
							if(flag4==false){
	/*good*/				//当按下=号后还可以按负根方，当小文本条最后一位为数字且小文本条不为0再按+-*/会把最新的计算结果推到小文本条中
								display1.setText(display.getText() + e.getActionCommand());  //最新的计算结果推到小文本条中
								sum=Double.parseDouble(display.getText());       //这个不能忘了，是为了必须是经过负根方的sum
								flag = true;                               //记得
								flag4=true;                        //记得
								lastNum = e.getActionCommand(); // 别忘了
							}
							else{
							
								display1.setText(display1.getText() + e.getActionCommand()); //只把计算符号推上去
								flag = true; // 记得弄回来						
								lastNum = e.getActionCommand(); // 别忘了
							}
						}
					}
				}
			else if(lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷")){
				display1.setText(display1.getText().substring(0,display1.getText().length()-1)+e.getActionCommand());
				lastNum=e.getActionCommand();                   //当按了+后再按-可以变换计算符号
			}
			}
//
//!!!		
		
		
		
			
	/*当你按下=时*/		
		else if (e.getActionCommand().equals("=")) {
			
			if (lastNum.matches("[0123456789]{1}") || lastNum.equals(".")) {
			//当上一个按钮为0123456789或.，不包括=号
				if (!(display1.getText().equals("0"))) {   //当小文本条为0时按下=号无效
					if (lastNum.equals(".")) {  //如果是点，当lastNum为“.”时，若出现“98.”,保存“98”，之后按了“+”，会输出98+而不是98.+
						display.setText(Smain);  //把文本条的“.”掉,即用没有.保存下来的Smain		
					}
					 if(Fmain.equals("÷")&&display.getText().matches("[0.]+")){
					    	display.setText("分母为0错.请按C");          
							setiEdit();
					  }
					 else{
					display1.setText(display1.getText() + display.getText());//把小文本条的数字放上去小文本条
					mid = Double.parseDouble(display.getText());        //下面算法与按下+-*/的一样
					 
					if (Fmain.equals("+")) {
						sum = sum + mid;
					} 
					else if (Fmain.equals("-")) {
						sum = sum - mid;
					} 
					else if (Fmain.equals("×")) {
						sum = sum * mid;
					} 
					else if (Fmain.equals("÷")) {
						sum = sum / mid;
					}
					if (String.valueOf(sum).length() >= 8) {
						BigDecimal sumM = new BigDecimal(sum);
						sum = sumM.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
						if((sum)%1.0==0){
							display.setText(String.valueOf((long)(sum)));
						}
						else{
							display.setText(String.valueOf(sum));
						}
					} 
					else {
						if ((sum) % 1.0 == 0) {
							display.setText(String.valueOf((long) (sum)));  //sum=(long) (sum)居然没作用
						} 
						else {
							display.setText(String.valueOf(sum));
						}
					}
					flag = false;        //当按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到小文本条,
					 }
				}

				lastNum = "09"; //为什么=是09,/*当你按下+-×÷时*/,if(lastNum.matches(regexN)||lastNum.equals("无")||lastNum.equals(".")){
				//如果上一个按钮是数字包括等号，开根，或者点时,这里matches(regexN)比较方便，当然也可以另写，但是一开始写了后来不想改了
			}

		    else if(lastNum.equals("无")){           //当按了根号以后再按等号
				if((display1.getText()).substring((display1.getText()).length()-1).matches("[0123456789]{1}")){
		/*good*/		//当一开始输入6再按平方再按等号时不会有反应，当1+1=2后按平方再按等号时不会有反应
				}
				else{//正常情况按了+16，把16平方
				display1.setText(display1.getText() + display.getText());  //以下算法与上文相同
				mid = Double.parseDouble(display.getText());
				if (Fmain.equals("+")) {
					sum = sum + mid;
				} 
				else if (Fmain.equals("-")) {
					sum = sum - mid;
				} 
				else if (Fmain.equals("×")) {
					sum = sum * mid;
				} 
				else if (Fmain.equals("÷")) {
					sum = sum / mid;
				}
				if (String.valueOf(sum).length() >= 8) {
					BigDecimal sumM = new BigDecimal(sum);
					sum = sumM.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
					if((sum)%1.0==0){
						display.setText(String.valueOf((long)(sum)));
					}
					else{
						display.setText(String.valueOf(sum));
					}
				} 
				else {
					if ((sum) % 1.0 == 0) {
						sum=(long)sum;
						display.setText(String.valueOf((long) (sum)));
					} 
					else {
						display.setText(String.valueOf(sum));
					}
				}
				flag = false;     //当按下“="后被设为false,此时若想继续进行计算，按下+-×÷后只会把+-×÷放到小文本条,
				lastNum = "09";    //别忘了
			}
				
		}
		}
//
//!!!
	
		
	
		
		
	/*当你按下C时*/		
		else if((e.getActionCommand()).equals("C")){  //重置一切
			display1.setText("0");
			sum=0;
			display.setText("0");
			lastNum="0";
			flag = true;    //不然就会1+1=后按C重置但是flag仍然是false,出现1++的情况
			flag3=true;     //这个要记得，忘了看上文
			flag4=true;     //别忘了
			Fmain="";
			Smain="";      //虽然不太造成什么影响，但是还是加上吧
/*good*/    setEdit();    //当输入错误，如分母为0，负数开根时按钮不可按按C后恢复
		}
//
//!!!		
	
		
		
		
	/*当你按下CE时*/	
		else if(e.getActionCommand().equals("CE")){ //一个很神奇的按钮
			if(lastNum.equals("09")||display1.getText().equals("0")){
 /*good*/		//当上一个按钮为=或者当小文本条为0即还没开始算时按这个就跟按C一样
				display1.setText("0");
				sum=0;
				display.setText("0");
				lastNum="0";
				flag = true;    //不然就会1+1=后按C重置但是flag仍然是false,出现1++的情况
				flag3=true;
				flag4=true;     //别忘了
				Fmain="";
				Smain="";      //虽然不太造成什么影响，但是还是加上吧
				setEdit();       //其实可以不要
			}
			else if(lastNum.matches("[0123456789]{1}")){ //当按了1+1此时按了CE，会把大文本条设为0，此时已将第一个1储存
				display.setText("0");
				lastNum="0";
				flag3=false;
			}
			else if(lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷")){
				sum= Double.parseDouble(display.getText());   //与上面else if关键就在于有没有储存没被删除的数字，这里没把第一个1储存，我们要完成它
				Fmain=lastNum;     //对比上面那个你会发现这两步就是按了1+1的最后一个1后的方法搬下来的，让他继续沿着轨道走
				display.setText("0");
				lastNum="0";
				flag3=false;       //什么都不能忘了
			}
			else if(lastNum.equals(".")){    //当按下9+再按点是不会有反应的，这里讨论的不是这个，这里讨论的是按下数字后再按点其实已经储存完毕了
				display.setText("0");
				lastNum="0";
				flag3=false;
			}
			else if(lastNum.equals("无")){    //这些都是储存了的，类似else if(lastNum.matches("[0123456789]{1}"))，其实可以写在一起，但是后来懒了
				display.setText("0");
				lastNum="0";
				flag3=false;
			}
			
			
		}
//
//!!!		
		
		
		
		
	/*当你按下.时*/	
		else if(e.getActionCommand().equals(".")){ //前两个if把它想成按了等号后按根负或计算符号后按了0后按了.
			    if((lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷"))){
			    	//当你按了+号后按".",大文本条会变成“0.”
			    	sum= Double.parseDouble(display.getText());    //这个就是前两个if把它想成按了等号后按根负或计算符号后按了0后按了.，所以sum不能漏，因为没储存上一个数
                    display.setText("0.");           //把按下的数字展示出来替换掉原来的
                    Fmain=lastNum;                                //把计算符号留起来
                    lastNum=".";
                    Smain="0";   //这个就是事实，Smain=display.getText();在下面
                    
                    }                 
			    else if(display1.getText().substring(display1.getText().length()-1).matches("[0123456789]{1}")&&!(display1.getText().equals("0"))){//同时小文本条不能为0
					display1.setText("0");
					sum=0;
					display.setText("0.");  //重置同时把按的数字加上去
					lastNum=".";        //把按的数字当成lastNum。。
					flag=true;           //这里也要加，否则按1+1=后按CE后按8后按+9+9后出现++9 这里关键是按了=号后没有重置flag。。
	                flag3=true;
					Fmain="";
					Smain="0";
				}
				else if(((display.getText()).contains("."))){
	/*good*/  				//如果大本条里面已经有了.那么不做任何操作，即不能添加两个.
				}
				else{
	/*good*/        Smain=display.getText();   //因为当lastNum为“.”时，若出现“98.”,保存“98”，之后按了“+”，会输出98+而不是98.+
					display.setText(display.getText()+".");
					lastNum=".";
				}
			}
//
//!!!		
		
		
	/*当你按下+/-时*/	
		else if(e.getActionCommand().equals("+/-")){
			if(lastNum.matches("[0123456789.]")||lastNum.equals("09")||lastNum.equals("无")){      //lastNum.equals("09")这里要注意会不会产生什么bug
				if(!(display.getText().equals("0"))){            //当display上为0时不可加负号
				if(lastNum.equals(".")){             //如上
					display.setText(Smain);
				}
					
				if(display.getText().contains("-")){        //如果文本条有-号则变成正，否则
						display.setText((display.getText()).replace("-", ""));
					}
					
				else{
						display.setText("-"+display.getText());
					}
			
				}
				if((!(display1.getText().equals("0")))&&display1.getText().substring(display1.getText().length()-1).matches("[0123456789]{1}")){
					flag4=false;      //当按下=号后还可以按负根方，当小文本条最后一位为数字且小文本条不为0再按+-*/会把最新的计算结果推到小文本条中
					//比如按了1+1=2把2变负再点加减乘除，小本条变成-2+，把原本的代替掉
				}
				
			
			}
			
		}
//
//!!!		
	
		
		
		
	/*当你按下＜dele时*/	
		else if(e.getActionCommand().equals("＜dele")){ //只有你在输入数字时才可以删除上一个输入的，上一个按钮为其他时dele键没有用，并且最多只能删为0
			if(!(display.getText().equals("0"))){
				//为0时不作操作
				if(display.getText().matches("[0123456789]{1}")){
					display.setText("0");
				}
				else{
					if(lastNum.equals("09")||lastNum.equals("无")||lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷")){
						
					}
					else{
						if(lastNum.equals(".")){
							lastNum="1";     //可以删掉“.”，这里的1其实没什么关系,lastNum为数字时是什么数字不重要
						}
						display.setText((display.getText()).substring(0,(display.getText()).length()-1));//即删除操作
					}
					
				}
			}
		}
//
//!!!		
		
		
		
		
	/*当你按下根方分之一时*/	
		else if(e.getActionCommand().equals("1/x")||e.getActionCommand().equals("X根")||e.getActionCommand().equals("X方")){
			if(!(lastNum.equals("+")||lastNum.equals("-")||lastNum.equals("×")||lastNum.equals("÷"))){
				//如果上一个键是计算符号，那么不进行一切操作
				if(lastNum.equals(".")){
					display.setText(Smain);         //如上
				}
				mid = Double.parseDouble(display.getText());         
				if (e.getActionCommand().equals("1/x")) {
					if (mid == 0) {
						display.setText("分母为0错.请按C");    //错误处理
						setiEdit();              //只能按C
					} 
					else {
						if (String.valueOf(1 / mid).length() >= 8) {           //以下输出方法上文有讲
							BigDecimal sumM = new BigDecimal(1 / mid);
							mid = sumM.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
							if((mid)%1.0==0){
								display.setText(String.valueOf((long)(mid)));
							}
							else{
								display.setText(String.valueOf(mid));
							}
						} 
						else {
							if((1 / mid)%1.0==0){
								display.setText(String.valueOf((long)(1 / mid)));
							}
							else{
								display.setText(String.valueOf(1 / mid));
							}
						}
					}
				}
				else if (e.getActionCommand().equals("X根")) {
					if(mid<0){
						display.setText("负数求根错.请按C");           //错误处理
						setiEdit();
					}
					else{
					if (String.valueOf(Math.sqrt(mid)).length() >= 8) {             //如上
						BigDecimal sumM = new BigDecimal(Math.sqrt(mid));
						mid = sumM.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
						if((mid)%1.0==0){
							display.setText(String.valueOf((long)(mid)));
						}
						else{
							display.setText(String.valueOf(mid));
						}
					} 
					else {
						if(Math.sqrt(mid)%1.0==0){
							display.setText(String.valueOf((long)Math.sqrt(mid)));
						}
						else{
							display.setText(String.valueOf(Math.sqrt(mid)));
						}
					}
					}
				} 
				else if (e.getActionCommand().equals("X方")) {            //如上
					if (String.valueOf(mid * mid).length() >= 8) {
						BigDecimal sumM = new BigDecimal(mid * mid);
						mid = sumM.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
						if((mid)%1.0==0){
							display.setText(String.valueOf((long)(mid)));
						}
						else{
							display.setText(String.valueOf(mid));
						}
					} 
					else {
						if((mid*mid)%1.0==0){
							display.setText(String.valueOf((long)(mid*mid)));
						}
						else{
							display.setText(String.valueOf(mid*mid));
						}
					}
				}
				if((!(display1.getText().equals("0")))&&display1.getText().substring(display1.getText().length()-1).matches("[0123456789]{1}")){
					flag4=false;
					//当按下=号后还可以按负根方，当小文本条最后一位为数字且小文本条不为0再按+-*/会把最新的计算结果推到小文本条中
					//比如按了1+1=2把2变方再点加减乘除，小本条变成4+，把原本的代替掉，若没点加减乘除，点了=，不会有反应，因为小文本条此时末尾为数字，=不会有操作，详见当你按下等号
				}
				lastNum = "无";       //为什么是无，我开心就行
			}
		}
	}
	
}//终于结束啦
