package java2020spring;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JTextPane;
/*本算法未参考任何他人的思路或代码，从无到有百分百纯原创。                   //创作者：李宪栋
本算法主要的思想是通过e.getActionCommand()为数字时，计算第二三个文本条所需要的数字，对三个文本条都进行set，展示出需要的文本。对于最后一个字符是.时进行了处理
以下代码将会给出详细注释以供参考*/    
public class ZhuanSuanFa implements ActionListener {
	double dis, dis2, dis3, dis4, dis5;
	JTextField display1;    
	JTextField display2;
	JTextPane display3;               //三个文本条
	String box1="米";      
	String box2="米";                   //两个下拉文本框但是是string,后面if语句比较好搞
	String regex = "[0123456789]+";

	public void setBox1(String box) {
		box1 = box;
	}

	public void setBox2(String box) {    //当下拉文本框被修改时会调用这个方法改过来
		box2 = box;
	}

	public void setJtext1(JTextField display) {
		display1 = display;
	}

	public void setJtext2(JTextField display) {
		display2 = display;
	}

	public void setJtext3(JTextPane display) {
		display3 = display;                           //把文本条调过来
	}          

	public void actionPerformed(ActionEvent e) {   
		if(e.getActionCommand().matches(regex)||e.getActionCommand().equals("＜dele")){  //按下数字或<dele时
			if(e.getActionCommand().matches(regex)){
				if(display1.getText().equals("0")){     //最开始是0时，按下数字会变成那个数字
					display1.setText(e.getActionCommand());
				}
				else{
					display1.setText(display1.getText()+e.getActionCommand());  //在后面加上数字
				}
			}
			else{   //为dele时
				if(display1.getText().matches("[0-9]{1}")){
					display1.setText("0");          //最多删为0
				}
				else{
					display1.setText((display1.getText()).substring(0,(display1.getText()).length()-1));//即删除操作
				}
			}
			if(!display1.getText().equals("0")||display1.getText().equals("0.")){
			     if(display1.getText().substring(display1.getText().length()-1).equals(".")){  //最后一位是.时忽略它
				dis=Double.parseDouble(display1.getText().substring(0,(display1.getText()).length()-1)); 
			 }
			    else{
				dis=Double.parseDouble(display1.getText());
			 }
			
			//以下就是两个下拉文本框不同对应情况下的反应。
             if(box1.equals("米")){
				if(box2.equals("公里")){
				    dis2=dis*0.001;
				}
				else if(box2.equals("英寸")){
					dis2=dis*39.37008;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3.28084;              
				}
				else if(box2.equals("码")){
					dis2=dis*1.093613;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000539957;                
				}
				else if(box2.equals("米")){
					dis2=dis;
				}
				dis3=(dis*1000);
				dis4=(dis*5.36);
				dis5=(dis*0.000621371);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sum1 = new BigDecimal(dis3);
				dis3 = sum1.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis3)+"毫米    "+String.valueOf(dis5)+"英里    "+  String.valueOf(dis4)+" 手   ");
				String s="约等于  \n"+String.valueOf(dis3)+"毫米   "+String.valueOf(dis5)+"英里    ";
				display3.setCaretPosition(s.length()+1);  // 设置插入位置 JTextPane可以插入图片
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));    // 插入图片
			}
			
			else if(box1.equals("公里")){
				if(box2.equals("公里")){
				    dis2=dis;
				}
				else if(box2.equals("米")){
					dis2=dis*1000;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3280.84;              
				}
				else if(box2.equals("码")){
					dis2=dis*1093.613;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.539957;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*39370.08;
				}
				
				dis4=(dis*0.621371);
				dis5=(dis*13.16);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用。
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 大型喷气客机");
				String s="约等于  "+String.valueOf(dis4)+"英里    ";
				display3.setCaretPosition(s.length());
				display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
			}
            
			else if(box1.equals("英寸")){
				if(box2.equals("公里")){
				    dis2=dis;
				}
				else if(box2.equals("米")){
					dis2=dis*0.0254;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*0.083333;              
				}
				else if(box2.equals("码")){
					dis2=dis*0.027778;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.0000137149;                
				}
				else if(box2.equals("英寸")){
					dis2=dis;
				}
				
				dis4=(dis*0.621371);
				dis5=(dis*0.72);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 曲别针");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("zhen.png")));
			}
            
			else if(box1.equals("英尺")){
				if(box2.equals("公里")){
				    dis2=dis*0.0003048;
				}
				else if(box2.equals("米")){
					dis2=dis*0.3048;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis;              
				}
				else if(box2.equals("码")){
					dis2=dis*0.333333;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000164579;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*12;
				}
				
				dis4=(dis*0.000189394);
				dis5=(dis*1.63);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
			}
            
			else if(box1.equals("码")){
				if(box2.equals("公里")){
				    dis2=dis*0.0009144;
				}
				else if(box2.equals("米")){
					dis2=dis*0.9144;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3;              
				}
				else if(box2.equals("码")){
					dis2=dis;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000493737;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*36;
				}
				
				dis4=(dis*0.000568182);
				dis5=(dis*4.9);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
			}
            
			else if(box1.equals("海里")){
				if(box2.equals("公里")){
				    dis2=dis*1.852;
				}
				else if(box2.equals("米")){
					dis2=dis*1852;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*6076.115;              
				}
				else if(box2.equals("码")){
					dis2=dis*2025.372;              
				}
				else if(box2.equals("海里")){
					dis2=dis;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*72913.39;
				}
				
				dis4=(dis*1.150779);
				dis5=(dis*24.37);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 大型喷气式客机");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
			}
			
			
		}
			else{
				display2.setText("0");
				display3.setText(" \n");
			}
		}
		
		
		else if(e.getActionCommand().equals("CE")){
			display1.setText("0");
			display2.setText("0");
			display3.setText("  \n");
		}
		
		else if(e.getActionCommand().equals(".")){
			if(display1.getText().contains(".")){
				
			}
			else
			   display1.setText(display1.getText()+".");
		}
	
	}
	

}

//以下就是下拉文本框的监听器，主要区别是会把改变后的box传给其他两个监听器，其他跟上面差不多
class ZhuanListen implements ItemListener{
	double dis, dis2, dis3, dis4, dis5;
	JTextField display1;
	JTextField display2;
	JTextPane display3;
	JComboBox boxe;
	String box1="米";
	String box2="米";
	String regex = "[0-9]+";
	ZhuanSuanFa suanFa;
	ZhuanListen2 listen;

	public void setBox1(JComboBox box) {
		boxe = box;
	}

	public void setBox2(String box) {
		box2 = box;
	}

	public void setJtext1(JTextField display) {
		display1 = display;
	}

	public void setJtext2(JTextField display) {
		display2 = display;
	}

	public void setJtext3(JTextPane display) {
		display3 = display;
	}
	public void setSuanFa(ZhuanSuanFa suan){
		suanFa=suan;
	}
	public void setListen(ZhuanListen2 lis){
		listen=lis;
	}


	public void itemStateChanged(ItemEvent e) {
		box1=boxe.getSelectedItem().toString();
		suanFa.setBox1(box1);
		listen.setBox1(box1);          //就是这里传
		if(!display1.getText().equals("0")||display1.getText().equals("0.")){
		if(display1.getText().substring(display1.getText().length()-1).equals(".")){
			dis=Double.parseDouble(display1.getText().substring(0,(display1.getText()).length()-1)); 
		}
		else{
			dis=Double.parseDouble(display1.getText());
		}
        if(box1.equals("米")){
			if(box2.equals("公里")){
			    dis2=dis*0.001;
			}
			else if(box2.equals("英寸")){
				dis2=dis*39.37008;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis*3.28084;              
			}
			else if(box2.equals("码")){
				dis2=dis*1.093613;              
			}
			else if(box2.equals("海里")){
				dis2=dis*0.000539957;                
			}
			else if(box2.equals("米")){
				dis2=dis;
			}
			dis3=(dis*1000);
			dis4=(dis*5.36);
			dis5=(dis*0.000621371);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sum1 = new BigDecimal(dis3);
			dis3 = sum1.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis3)+"毫米    "+String.valueOf(dis5)+"英里    "+  String.valueOf(dis4)+" 手   ");
			String s="约等于  \n"+String.valueOf(dis3)+"毫米   "+String.valueOf(dis5)+"英里    ";
			display3.setCaretPosition(s.length()+1);  // 设置插入位置 JTextPane可以插入图片
			display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));    // 插入图片
		}
		
		else if(box1.equals("公里")){
			if(box2.equals("公里")){
			    dis2=dis;
			}
			else if(box2.equals("米")){
				dis2=dis*1000;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis*3280.84;              
			}
			else if(box2.equals("码")){
				dis2=dis*1093.613;              
			}
			else if(box2.equals("海里")){
				dis2=dis*0.539957;                
			}
			else if(box2.equals("英寸")){
				dis2=dis*39370.08;
			}
			
			dis4=(dis*0.621371);
			dis5=(dis*13.16);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用。
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 大型喷气客机");
			String s="约等于  "+String.valueOf(dis4)+"英里    ";
			display3.setCaretPosition(s.length());
			display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
		}
        
		else if(box1.equals("英寸")){
			if(box2.equals("公里")){
			    dis2=dis;
			}
			else if(box2.equals("米")){
				dis2=dis*0.0254;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis*0.083333;              
			}
			else if(box2.equals("码")){
				dis2=dis*0.027778;              
			}
			else if(box2.equals("海里")){
				dis2=dis*0.0000137149;                
			}
			else if(box2.equals("英寸")){
				dis2=dis;
			}
			
			dis4=(dis*0.621371);
			dis5=(dis*0.72);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 曲别针");
			String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
			display3.setCaretPosition(s.length()+1);
			display3.insertIcon(new ImageIcon(this.getClass().getResource("zhen.png")));
		}
        
		else if(box1.equals("英尺")){
			if(box2.equals("公里")){
			    dis2=dis*0.0003048;
			}
			else if(box2.equals("米")){
				dis2=dis*0.3048;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis;              
			}
			else if(box2.equals("码")){
				dis2=dis*0.333333;              
			}
			else if(box2.equals("海里")){
				dis2=dis*0.000164579;                
			}
			else if(box2.equals("英寸")){
				dis2=dis*12;
			}
			
			dis4=(dis*0.000189394);
			dis5=(dis*1.63);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
			String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
			display3.setCaretPosition(s.length()+1);
			display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
		}
        
		else if(box1.equals("码")){
			if(box2.equals("公里")){
			    dis2=dis*0.0009144;
			}
			else if(box2.equals("米")){
				dis2=dis*0.9144;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis*3;              
			}
			else if(box2.equals("码")){
				dis2=dis;              
			}
			else if(box2.equals("海里")){
				dis2=dis*0.000493737;                
			}
			else if(box2.equals("英寸")){
				dis2=dis*36;
			}
			
			dis4=(dis*0.000568182);
			dis5=(dis*4.9);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
			String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
			display3.setCaretPosition(s.length()+1);
			display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
		}
        
		else if(box1.equals("海里")){
			if(box2.equals("公里")){
			    dis2=dis*1.852;
			}
			else if(box2.equals("米")){
				dis2=dis*1852;                 
			}
			else if(box2.equals("英尺")){
				dis2=dis*6076.115;              
			}
			else if(box2.equals("码")){
				dis2=dis*2025.372;              
			}
			else if(box2.equals("海里")){
				dis2=dis;                
			}
			else if(box2.equals("英寸")){
				dis2=dis*72913.39;
			}
			
			dis4=(dis*1.150779);
			dis5=(dis*24.37);
			if ((dis2) % 1.0 == 0) {
				display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
			} 
			else {
				display2.setText(String.valueOf(dis2));
			}
			BigDecimal sumM = new BigDecimal(dis5);
			dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			BigDecimal sum2 = new BigDecimal(dis4);
			dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 大型喷气式客机");
			String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
			display3.setCaretPosition(s.length()+1);
			display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
		}
		
	}
		else{
			display2.setText("0");
			display3.setText(" \n");
		}
	
		
	}
}


class ZhuanListen2 implements ItemListener{
	double dis, dis2, dis3, dis4, dis5;
	JTextField display1;
	JTextField display2;
	JTextPane display3;
	JComboBox boxe;
	String box1="米";
	String box2="米";
	String regex = "[0-9]+";
	ZhuanSuanFa suanFa;
	ZhuanListen listen;

	public void setBox2(JComboBox box) {
		boxe = box;
	}

	public void setBox1(String box) {
		box1 = box;
	}

	public void setJtext1(JTextField display) {
		display1 = display;
	}

	public void setJtext2(JTextField display) {
		display2 = display;
	}

	public void setJtext3(JTextPane display) {
		display3 = display;
	}
	public void setSuanFa(ZhuanSuanFa suan){
		suanFa=suan;
	}
	public void setListen(ZhuanListen lis){
		listen=lis;
	}


	public void itemStateChanged(ItemEvent e) {
		box2=boxe.getSelectedItem().toString();
		suanFa.setBox2(box2);
		listen.setBox2(box2);
		if(!display1.getText().equals("0")||display1.getText().equals("0.")){
		  if(display1.getText().substring(display1.getText().length()-1).equals(".")){
			dis=Double.parseDouble(display1.getText().substring(0,(display1.getText()).length()-1)); 
		 }
		  else{
			dis=Double.parseDouble(display1.getText());
		 }
          if(box1.equals("米")){
				if(box2.equals("公里")){
				    dis2=dis*0.001;
				}
				else if(box2.equals("英寸")){
					dis2=dis*39.37008;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3.28084;              
				}
				else if(box2.equals("码")){
					dis2=dis*1.093613;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000539957;                
				}
				else if(box2.equals("米")){
					dis2=dis;
				}
				dis3=(dis*1000);
				dis4=(dis*5.36);
				dis5=(dis*0.000621371);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sum1 = new BigDecimal(dis3);
				dis3 = sum1.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis3)+"毫米    "+String.valueOf(dis5)+"英里    "+  String.valueOf(dis4)+" 手   ");
				String s="约等于  \n"+String.valueOf(dis3)+"毫米   "+String.valueOf(dis5)+"英里    ";
				display3.setCaretPosition(s.length()+1);  // 设置插入位置 JTextPane可以插入图片
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));    // 插入图片
			}
			
			else if(box1.equals("公里")){
				if(box2.equals("公里")){
				    dis2=dis;
				}
				else if(box2.equals("米")){
					dis2=dis*1000;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3280.84;              
				}
				else if(box2.equals("码")){
					dis2=dis*1093.613;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.539957;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*39370.08;
				}
				
				dis4=(dis*0.621371);
				dis5=(dis*13.16);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用。
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 大型喷气客机");
				String s="约等于  "+String.valueOf(dis4)+"英里    ";
				display3.setCaretPosition(s.length());
				display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
			}
          
			else if(box1.equals("英寸")){
				if(box2.equals("公里")){
				    dis2=dis;
				}
				else if(box2.equals("米")){
					dis2=dis*0.0254;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*0.083333;              
				}
				else if(box2.equals("码")){
					dis2=dis*0.027778;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.0000137149;                
				}
				else if(box2.equals("英寸")){
					dis2=dis;
				}
				
				dis4=(dis*0.621371);
				dis5=(dis*0.72);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 曲别针");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("zhen.png")));
			}
          
			else if(box1.equals("英尺")){
				if(box2.equals("公里")){
				    dis2=dis*0.0003048;
				}
				else if(box2.equals("米")){
					dis2=dis*0.3048;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis;              
				}
				else if(box2.equals("码")){
					dis2=dis*0.333333;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000164579;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*12;
				}
				
				dis4=(dis*0.000189394);
				dis5=(dis*1.63);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
			}
          
			else if(box1.equals("码")){
				if(box2.equals("公里")){
				    dis2=dis*0.0009144;
				}
				else if(box2.equals("米")){
					dis2=dis*0.9144;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*3;              
				}
				else if(box2.equals("码")){
					dis2=dis;              
				}
				else if(box2.equals("海里")){
					dis2=dis*0.000493737;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*36;
				}
				
				dis4=(dis*0.000568182);
				dis5=(dis*4.9);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里    "+String.valueOf(dis5)+" 手");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("hand.png")));
			}
          
			else if(box1.equals("海里")){
				if(box2.equals("公里")){
				    dis2=dis*1.852;
				}
				else if(box2.equals("米")){
					dis2=dis*1852;                 
				}
				else if(box2.equals("英尺")){
					dis2=dis*6076.115;              
				}
				else if(box2.equals("码")){
					dis2=dis*2025.372;              
				}
				else if(box2.equals("海里")){
					dis2=dis;                
				}
				else if(box2.equals("英寸")){
					dis2=dis*72913.39;
				}
				
				dis4=(dis*1.150779);
				dis5=(dis*24.37);
				if ((dis2) % 1.0 == 0) {
					display2.setText(String.valueOf((long) (dis2)));  //sum=(long) (sum)居然没作用
				} 
				else {
					display2.setText(String.valueOf(dis2));
				}
				BigDecimal sumM = new BigDecimal(dis5);
				dis5 = sumM.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				BigDecimal sum2 = new BigDecimal(dis4);
				dis4 = sum2.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
				display3.setText("约等于  \n"+String.valueOf(dis4)+" 英里     "+String.valueOf(dis5)+" 大型喷气式客机");
				String s="约等于  \n"+String.valueOf(dis4)+" 英里   ";
				display3.setCaretPosition(s.length()+1);
				display3.insertIcon(new ImageIcon(this.getClass().getResource("fuck.png")));
			}
		
	}
		else{
			display2.setText("0");
			display3.setText(" \n");
		}
	}

}
